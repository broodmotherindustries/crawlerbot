import random as r


def action_task(hook_params):
    response_template = r.choice(['Sup ...', 'Yo!', 'Hola', 'Bonjour!'])
    return {'text': response_template, 'attachment': ''}
