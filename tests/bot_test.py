import sys
import uuid
import re
import unittest
import xmlrunner
from unittest.mock import patch
import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import Bot

class TestBot(unittest.TestCase):

    def setUp(self):
        self.bot = Bot.Bot()
        self.guid = str(uuid.uuid4())
        self.UUID_PATTERN = re.compile(r'^[\da-f]{8}-([\da-f]{4}-){3}[\da-f]{12}$', re.IGNORECASE)

    @patch('Bot.Bot.return_id', return_value=str(uuid.uuid4()))
    def test_id_is_UUID(self, return_id):
        is_id = False
        if self.UUID_PATTERN.match(return_id()):
            is_id = True
        else:
            is_id = False
        self.assertEqual(is_id, True)


if __name__ == '__main__':
    unittest.main(
            testRunner=xmlrunner.XMLTestRunner(open('../report.xml', 'w'), outsuffix = ''),
            failfast=False,
            buffer=False,
            catchbreak=False)