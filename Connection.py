import random


class Connection:

    def __init__(self, temp_weight=None):
        self.conn_entry = 0
        self.weight = 0
        self.conn_exit = 0
        if temp_weight is None:
            self.randomiseWeight()
        else:
            self.set_weight(temp_weight)

    def randomise_weight(self):
        self.set_weight(random.random())

    def set_weight(self, temp_weight):
        self.weight = temp_weight

    def calc_conn_exit(self, temp_input):
        self.conn_entry = temp_input
        self.conn_exit = self.conn_entry * self.weight
