from Shared import Config
from Bot import Bot
import json
from socketIO_client import SocketIO, BaseNamespace


class ClientNamespace(BaseNamespace):
    BaseNamespace.bot = Bot()

    def on_connect(self):
        self.emit("NEW BOT", json.dumps(self.bot.details))
        self.bot.bot_Updated += self.BotLegMoved()

    def on_disconnect(self):
        return "disconnected"

    def on_reconnect(self):
        print("reconnected")
        self.emit("NEW BOT", json.dumps(self.bot.details))

    def on_command(self, data):
        print('on_command' + str(data))

    def on_message(self, data):
        instruction = data.decode('utf-8').split(',', 1)
        command = instruction[0].replace('2', '').replace('[', '').replace('"', '')
        info = None
        print(command)
        if (command is not '0'):
            if len(instruction) > 1:
                current_data = instruction[1].replace('\\', '').replace(']', '').replace('"', '', 1)[:-1]
                if self.is_json(current_data):
                    info = json.loads(current_data)
            self.parse_command(command, info)

    def parse_command(self, command, data):
        results = self.bot.new_command(command, data)
        self.emit("command_results", results)
        pass

    def BotLegMoved(self):
        self.emit("UPDATE BOT", json.dumps(self.bot.details))

    def is_json(self, myjson):
        try:
            json_object = json.loads(myjson)
        except ValueError as e:
            print(e)
            return False
        return True


if __name__ == "__main__":
    config = Config()

    socketIO = SocketIO(host=config.WEBSOCKET_URL, port=config.WEBSOCKET_PORT, Namespace=ClientNamespace)
    clientSocket = socketIO.define(ClientNamespace, '/')
    socketIO.wait()
