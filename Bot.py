import uuid
import random

from BotCore.Outputs.SunfounderPCA9685 import SunfounderPCA9685
from Shared import EventHook
from Shared import plugin_manager
import sys



class MyDict(dict):

    def __init__(self, *arg, **kw):
        super(MyDict, self).__init__(*arg, **kw)
        self.bot_Updated = EventHook()

    def __setitem__(self, item, value):
        self.bot_Updated.fire()
        super(MyDict, self).__setitem__(item, value)


class Bot:

    def __init__(self):
        self.plugin_manager = plugin_manager.PluginManager(plugin_folder='./plugins')
        self.bot_Updated = EventHook();
        self.details = MyDict({
            'friendlyName': 'crawler-bot-' + str(random.randrange(50)),
            'guid': str(uuid.uuid4()),
            'x':  random.randrange(50),
            'y':  random.randrange(50),
            'z': random.randrange(50),
            'legs': MyDict({
                'front_left': MyDict({
                    'hip': MyDict({
                        'rotation': 0,
                        'id': 0
                    }),
                    'leg': MyDict({
                        'rotation': 0,
                        'id': 1
                    }),
                    'foot': MyDict({
                        'rotation': 0,
                        'id': 2
                    })
                }),
                'front_right': MyDict({
                    'hip': MyDict({
                        'rotation': 0,
                        'id': 4
                    }),
                    'leg': MyDict({
                        'rotation': 0,
                        'id': 5
                    }),
                    'foot': MyDict({
                        'rotation': 0,
                        'id': 6
                    })
                }),
                'back_left': MyDict({
                    'hip': MyDict({
                        'rotation': 0,
                        'id': 8
                    }),
                    'leg': MyDict({
                        'rotation': 0,
                        'id': 9
                    }),
                    'foot': MyDict({
                        'rotation': 0,
                        'id': 10
                    })
                }),
                'back_right': MyDict({
                    'hip': MyDict({
                        'rotation': 0,
                        'id': 12
                    }),
                    'leg': MyDict({
                        'rotation': 0,
                        'id': 13
                    }),
                    'foot': MyDict({
                        'rotation': 0,
                        'id': 14
                    })
                })

            })
        })
        self.servos = SunfounderPCA9685(self.details['legs'])
        self.details['legs']['front_left']['hip']['rotation'] = 1
        self.details.bot_Updated += self.send_update()

    def send_update(self):
        self.bot_Updated.fire()

    def return_id(self):
        return self.details.guid

    def new_command(self, command, value):
        if command in self.plugin_manager.get_available_plugins():
            self.plugin_manager.load_plugin(command)
            result = self.plugin_manager.execute_action_hook('task', { 'bot': self.details, 'value': value})
            self.plugin_manager.unload_plugin(command)
            return result
        return ''

if __name__== "__main__":
    Bot()